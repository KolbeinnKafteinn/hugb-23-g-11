import grpc
import sys
import os

from grpc_helpers import mood_music_service_pb2 as mood_music_service_pb2
from grpc_helpers import mood_music_service_pb2_grpc as mood_music_service_pb2_grpc

current_dir = os.path.dirname(__file__)
project_dir = os.path.dirname(current_dir)
sys.path.append(project_dir)

def delete_playlist(stub, playlist_id:str):
    """
    Args:
        stub (Stub Obj) : Stub object from grpc framework
        playlist_id (str) : id of playlist
    """
    # Create delete playlist request
    request = mood_music_service_pb2.delete_playlist_request(playlist_id=playlist_id)

    # Save delete playlist response
    response = stub.delete_playlist(request)

    # Print message from response
    print(response.message)

def delete_user(stub, user_id:str):
    """
    Args:
        stub (Stub Obj) : Stub object from grpc framework
        user_id (str) : id of user        
    """
    # Create delete user request
    request = mood_music_service_pb2.delete_user_request(user_id=user_id)

    # Save delete user response
    response = stub.delete_user(request)

    # Print message from response
    print(response.message)

def register_playlist(stub, user_id:str, title:str, mood:str, private:bool):
    """
    Args:
        stub (Stub Obj) : Stub object from grpc framework
        user_id (str) : id of user
        title (str) : title of new playlist
        mood (str) : mood of the new playlist
        private (bool) : privacy status of playlist
    """
    # Create register playlist request
    request = mood_music_service_pb2.register_playlist_request(user_id=user_id, title=title, mood=mood, private=private)

    # Save register playlist response
    response = stub.register_playlist(request)

    # Print message from response
    print(response.message)

def register_song(stub, title:str, artist:str, mood:str, lyrics:str):
    """
    Args:
        stub (Stub Obj) : Stub object from grpc framework
        title (str) : title of new song
        artist (str) : name of artist
        mood (str) : mood of song
        lyrics : (str) : string representaion of lyrics
    """
    # Create register song request
    request = mood_music_service_pb2.register_song_request(title=title, artist=artist, mood=mood, lyrics=lyrics)

    # Save register song response
    respone = stub.register_song(request)

    # Print message from response
    print(respone.message)

def register_user(stub, name:str, username:str, email:str, password:str):
    """
    Args:
        stub (Stub Obj) : Stub object from grpc framework
        name (str) : name of user
        username (str) : online handle of user
        email (str) : email of user
        password (str) : password of user        
    """
    # Create register user request
    request = mood_music_service_pb2.register_user_request(name=name, username=username, email=email, password=password)

    # Save register user response
    response = stub.register_user(request)

    # Print message from response
    print(response.message)

def register_timeline(stub, user_id:str):
    """
    Args:
        stub (Stub Obj) : Stub object from grpc framework
        user_id (str) : id of user
    """
    # Create register timeline request
    request = mood_music_service_pb2.register_timeline_request(user_id=user_id)

    # Save register timeline response
    response = stub.register_timeline(request)

    # Print out message from reponse
    print(response.message)

def log_mood(stub, user_id:str, song_id:str):
    """
    Args:
        stub (Stub Obj) : Stub object from grpc framework
        user_id (str) : id of user
        song_id (str) : id of song        
    """
    # Create log_mood request
    request = mood_music_service_pb2.log_mood_request(user_id=user_id, song_id=song_id)

    # Save log_mood response
    response = stub.log_mood(request)

    # Print out message from response
    print(response.message)

def share_playlist(stub, user_id:str, playlist_id:str):
    """
    Args:
        stub (Stub Obj) : Stub object from grpc framework
        user_id (str) : id of user
        playlist_id (str) id of playlist
    """
    # Create share playlist request
    request = mood_music_service_pb2.share_playlist_request(user_id=user_id, playlist_id=playlist_id)

    # Save share_playlist response
    response = stub.share_playlist(request)

    # Print out message from response
    print(response.message)

def recommend_playlist(stub, user_id:str, mood:str, title:str):
    """
    Args:
        stub (Stub Obj) : Stub object from grpc framework
        user_id (str) : id of user
        mood (str) : mood of playlist you want to reccomend
        title (str) : title of the playlist you want to reccomend        
    """
    # Create reccomend_playlist request
    request = mood_music_service_pb2.recommend_playlist_request(user_id=user_id, title=title, mood=mood)

    # Save reccomend_playlist response
    response = stub.recommend_playlist(request)

    # Print out message from resposne
    print(response.message)

    

def view_timeline(stub, user_id:str):
    """
    Args:
        stub (Stub Obj) : Stub object from grpc framework
        user_id (str) : id of user
    """
    # Create reccomend_playlist request
    request = mood_music_service_pb2.view_timeline_request(user_id=user_id)

    # Save reccomend_playlist response
    response = stub.view_timeline(request)

    # Print out message from resposne
    print(response.message)

def create_mood_based_playlist(stub, name:str, mood:str):
    """
    Sends a request to the gRPC service to create a mood-based playlist for a user.

    This function prepares a request with the user's ID and desired mood, sends it
    to the gRPC server using the provided stub, and prints out the server's response.

    Args:
        user_id (str): The ID of the user for whom the playlist is being created.
        mood (str): The mood based on which the playlist is to be created.

    Returns:
        None: This function does not return a value but prints the response message.
    """
    request = mood_music_service_pb2.create_mood_based_playlist_request(name=name, mood=mood)

    response = stub.create_mood_based_playlist(request)

    print(response.message)

def add_song_to_playlist(stub, playlist_id: str, song_id: str):
    """
    Sends a request to the gRPC service to add a song to a specific playlist.

    This function prepares a request with a playlist ID and a song ID, sends it to
    the gRPC server using the provided stub, and prints out the server's response.

    Args:
        playlist_id (str): The ID of the playlist to which the song will be added.
        song_id (str): The ID of the song to add to the playlist.

    Returns:
        None: This function does not return a value but prints the response message.
    """
    request = mood_music_service_pb2.add_song_to_playlist_request(playlist_id=playlist_id, song_id=song_id)

    response = stub.add_song_to_playlist(request)

    print(response.message)

def view_playlist(stub, playlist_id:str):
    """
    Args:
        stub (Stub Obj) : Stub object from grpc framework
        playlist_id (str) : id of playlist
    """
    # Create reccomend_playlist request
    request = mood_music_service_pb2.view_playlist_request(playlist_id=playlist_id)

    # Save reccomend_playlist response
    response = stub.view_playlist(request)

    # Print out message from resposne
    print(response.message)

def view_song(stub, song_id:str):
    """
    Args:
        stub (Stub Obj) : Stub object from grpc framework
        song_id (str) : id of song
    """
    # Create reccomend_playlist request
    request = mood_music_service_pb2.view_song_request(song_id=song_id)

    # Save reccomend_playlist response
    response = stub.view_song(request)

    # Print out message from resposne
    print(response.message)

def view_user(stub, user_id:str):
    """
    Args:
        stub (Stub Obj) : Stub object from grpc framework
        user_id (str) : id of user
    """
    # Create reccomend_playlist request
    request = mood_music_service_pb2.view_user_request(user_id=user_id)

    # Save reccomend_playlist response
    response = stub.view_user(request)

    # Print out message from resposne
    print(response.message)

def change_mood(stub, user_id:str, mood:str):
    """
    Args:
        stub (Stub Obj) : Stub object from grpc framework
        user_id (str) : id of user
        mood (str) : mood
    """
    # Create reccomend_playlist request
    request = mood_music_service_pb2.change_mood_request(user_id=user_id, mood=mood)

    # Save reccomend_playlist response
    response = stub.change_mood(request)

    # Print out message from resposne
    print(response.message)

def main():
    # Set Current working directory to project dir
    os.chdir(project_dir)

    # Connect to the gRPC server
    channel = grpc.insecure_channel('localhost:50051')  # Update with the server address and port

    mood_music_stub = mood_music_service_pb2_grpc.MoodMusicServiceStub(channel)

    # Call gRPC methods
    # USER METHODS
    print("Testing register user method")
    register_user(mood_music_stub, "John Doe", "johndoe", "johndoe@email.com", "password123")
    register_user(mood_music_stub, "Jane Doe","janedoe","janedoe@email.com","password123")
    register_user(mood_music_stub, "Hrafnkell Orri Elvarsson", "Hasteyx", "hrafnkell@email.com", "password123")
    register_user(mood_music_stub, "Kristinn Anton Hallvarðsson", "Krissi", "kristinn.ha@email.com", "pass111")
    register_user(mood_music_stub, "kolli", "kall", "icehot@hotmail.com", "ka12321")
    register_user(mood_music_stub, "klli", "kall", "icehot@hotmail.com", "ka12321")

    print("\n\nTesting register user method ERROR MESSAGE!")
    register_user(mood_music_stub, "", "", "", "")

    print("\n\nTesting change mood method")
    change_mood(mood_music_stub, 'Krissi', 'Happy')
    change_mood(mood_music_stub, 'Hasteyx', 'Angry')
    change_mood(mood_music_stub, 'kall', 'Agressive')

    print("\n\nTesting change mood method ERROR MESSAGE!")
    change_mood(mood_music_stub, 'kolli', 'Sad')


    print('\n\nTesting view user method')
    view_user(mood_music_stub, '1')
    view_user(mood_music_stub, '2')
    view_user(mood_music_stub, '3')
    view_user(mood_music_stub, '4')

    print('\n\nTesting view user method ERROR MESSAGE!')
    view_user(mood_music_stub, '-10')


    # PLAYLIST AND SONG METHODS
    print("\n\nTesting register playlist method")
    register_playlist(mood_music_stub, "1", "$uicideboy$ MIX #1", "Agressive", True)
    register_playlist(mood_music_stub, "2", "Classics", "Sad", True)
    register_playlist(mood_music_stub, "3", "Sad mix", "Sad", True)
    register_playlist(mood_music_stub, "4", "Happy Mix", "Happy", True)

    print("\n\nTesting register playlist method ERROR MESSAGE!")
    register_playlist(mood_music_stub, "", "", "Happy", True)

    print("\n\nTesting register song method")
    register_song(mood_music_stub, "Everlong", "Foo Fighters", "Sad", "")
    register_song(mood_music_stub, "This Fffire", "Franz Ferdinand", "Agressive", "")
    register_song(mood_music_stub, "Phantom Menace", "$uicideboy$", "Angry", "")
    register_song(mood_music_stub, "Avalon", "$uicideboy$", "Angry", "")
    register_song(mood_music_stub, "Matte Black", "$uicideboy$", "Sad", "")
    register_song(mood_music_stub, "Bloody 98", "$uicideboy$", "Angry", "")
    register_song(mood_music_stub, "Lover is a Day", "Cuco", "Sad", "")
    register_song(mood_music_stub, "rises the moon", "liana flores", "Sad", "")
    register_song(mood_music_stub, "Casio", "Jungle", "Happy", "")
    register_song(mood_music_stub, "She's my collar","Gorillaz","Happy","")
    register_song(mood_music_stub, "Virtual Insanity","Jamoriquai","Happy","")
    register_song(mood_music_stub, "Mixed Messages","tom cardy","Happy","")
    register_song(mood_music_stub, "Red Flags","tom cardy","Happy","")
    register_song(mood_music_stub, "Mississippi Queen","Mountain","Happy","")
    register_song(mood_music_stub, "Messages from the Start","The RAH Band","Happy","")
    register_song(mood_music_stub, "Eyes Without A Face","Billy Idol","Happy","")
    register_song(mood_music_stub, "draco","playboy carti","Happy","")

    print("\n\nTesting register song method ERROR MESSAGE!")
    register_song(mood_music_stub, "","","","")

    print('\n\nTesting add song to playlist method')
    add_song_to_playlist(mood_music_stub, 'Classics', 'Everlong')
    add_song_to_playlist(mood_music_stub, 'Happy Mix', 'Virtual Insanity')
    add_song_to_playlist(mood_music_stub, '$uicideboy$ MIX #1', 'draco')
    add_song_to_playlist(mood_music_stub, '$uicideboy$ MIX #1', 'Everlong')
    add_song_to_playlist(mood_music_stub, '$uicideboy$ MIX #1', 'Casio')
    add_song_to_playlist(mood_music_stub, '$uicideboy$ MIX #1', 'Mississippi Queen')
    add_song_to_playlist(mood_music_stub, '$uicideboy$ MIX #1', 'Eyes Without A Face')
    add_song_to_playlist(mood_music_stub, '$uicideboy$ MIX #1', 'Lover is a Day')

    print('\n\nTesting add song to playlist method ERROR MESSAGE!')
    add_song_to_playlist(mood_music_stub, 'No_Song', 'No_Playlist')

    print('\n\nTesting view song method')
    view_song(mood_music_stub, '1')
    view_song(mood_music_stub, '2')
    view_song(mood_music_stub, '3')
    view_song(mood_music_stub, '4')
    view_song(mood_music_stub, '5')
    view_song(mood_music_stub, '6')
    view_song(mood_music_stub, '7')
    view_song(mood_music_stub, '8')
    view_song(mood_music_stub, '9')
    view_song(mood_music_stub, '10')
    view_song(mood_music_stub, '11')
    view_song(mood_music_stub, '12')
    view_song(mood_music_stub, '13')
    view_song(mood_music_stub, '14')
    view_song(mood_music_stub, '15')
    view_song(mood_music_stub, '16')
    view_song(mood_music_stub, '17')

    # DO NOT BE ALARMED, THIS BREAKS BUT RETURNS A PROPERLY FORMATTED ERROR MESSAGE
    print('\n\nTesting view song method ERROR MESSAGE!')
    view_song(mood_music_stub, '18')

    print('\n\nTesting view playlist method')
    view_playlist(mood_music_stub, '1')

    print('\n\nTesting view playlist method ERROR MESSAGE!')
    view_playlist(mood_music_stub, 'noPlaylist')

    print("\n\nTesting sharing playlist method")
    share_playlist(mood_music_stub, '1', '1')
    share_playlist(mood_music_stub, '2', '2')
    share_playlist(mood_music_stub, '3', '3')

    print("\n\nTesting sharing playlist method ERROR MESSAGE!")
    share_playlist(mood_music_stub, '100', '-100')

    print("\n\nTesting for recommend playlist method")
    recommend_playlist(mood_music_stub, "Krissi", "Happy", "Happy Mix")
    recommend_playlist(mood_music_stub, "Hasteyx", "Sad", "Sad mix")
    recommend_playlist(mood_music_stub, "johndoe", "Sad", "Classics")

    print("\n\nTesting for recommend playlist method ERROR MESSAGE!")
    recommend_playlist(mood_music_stub, "Nobody", "Happy", "Happy Mix")

    #print("\n\nTesting create mood based playlist method")
    #create_mood_based_playlist(mood_music_stub, 'Happy', 'happy')
    #create_mood_based_playlist(mood_music_stub, '6', 'Angry')
    #create_mood_based_playlist(mood_music_stub, '1', 'Sad')

    # TIMELINE METHODS
    print("\n\nTesting register timeline method")
    register_timeline(mood_music_stub, "1")
    register_timeline(mood_music_stub, "2")
    register_timeline(mood_music_stub, "3")
    print("\n\nTesting register timeline method ERROR MESSAGE!")
    register_timeline(mood_music_stub, "")

    print("\n\nTesting log mood method") # DO MORE ENTRIES, WITH DIFFERENT TIMES (hint use time.wait() or something similar)
    log_mood(mood_music_stub, "kall", "1")
    log_mood(mood_music_stub, "Krissi", "2")
    log_mood(mood_music_stub, "Hasteyx", "3")

    print("\n\nTesting log mood method ERROR MESSAGE!")
    log_mood(mood_music_stub, "10000", "4")

    print("\n\nTesting view timeline method")
    view_timeline(mood_music_stub, "Krissi")
    view_timeline(mood_music_stub, "Hasteyx")
    view_timeline(mood_music_stub, "kall")

    print("\n\nTesting view timeline method ERROR MESSAGE!")
    view_timeline(mood_music_stub, '1000')

    
    print("\nTesting delete playlist method")
    delete_playlist(mood_music_stub, "Classics")
    delete_playlist(mood_music_stub, "$uicideboy$ MIX #1")
    delete_playlist(mood_music_stub, "Sad mix")
    delete_playlist(mood_music_stub, "Happy Mix")


    print("\nTesting delete playlist method ERROR MESSAGE!")
    delete_playlist(mood_music_stub, "NotApLaYlIsT")

    print("\nTesting delete user method")
    delete_user(mood_music_stub, "janedoe")
    delete_user(mood_music_stub, "kall")
    

    print("\nTesting delete user method ERROR MESSAGE!")
    delete_user(mood_music_stub, "eitthva[rugl]")

    

if __name__ == '__main__':
    main()
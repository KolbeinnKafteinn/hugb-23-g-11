import grpc
import sys
import os

project_dir = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))
sys.path.append(project_dir)

from classes.mood_music import MoodMusic
from grpc_helpers import mood_music_service_pb2 as mood_music_service_pb2
from grpc_helpers import mood_music_service_pb2_grpc as mood_music_service_pb2_grpc

from grpc_helpers import weather_service_pb2 as weather_service_pb2
from grpc_helpers import weather_service_pb2_grpc as weather_service_pb2_grpc

from concurrent.futures import ThreadPoolExecutor

current_dir = os.path.dirname(__file__)
project_dir = os.path.dirname(current_dir)
sys.path.append(project_dir)


class WeatherServicer():
    def __init__(self) -> None:
        self.API_KEY = '6ee030c8-e16c-48d0-bfc3-3c0a23191241'
        self.SERVICE_URL = 'hugb-service-ep3thvejnq-uc.a.run.app:443'

    def get_current_weather(self) -> list:
        """Gets current weather from location
        Returns:
            list: list of weather attributes
        """

        # current_weather to none incase of function failure
        current_weather = None

        # Attempt to communicate to server and request information
        try:
            # Create Sercure connecton to service
            with grpc.secure_channel(self.SERVICE_URL, grpc.ssl_channel_credentials()) as channel:
                
                # Co-ordinates of Reykjavik City
                lat = 64.13
                lon = -21.93

                # Create grpc stub
                stub = weather_service_pb2_grpc.WeatherStationStub(channel=channel)

                # Get current_weather
                current_weather = stub.get_current_weather(weather_service_pb2.location_request(lat=lat, lon=lon, api_key=self.API_KEY))

                return current_weather

        except grpc.RpcError as e:
            # Get necessary information about error
            status_code = e.code()

            # Print out relevant information
            print(e.details())
            print(status_code.name)
            print(status_code.value)

        except Exception as e:
            # Print out relevant information
            print(e)

            # Assume grpcs didnt break and return current weather as is
            return current_weather


class MoodMusicServicer(mood_music_service_pb2_grpc.MoodMusicServiceServicer):
    def __init__(self, weather_servicer):
        self.mood_music = MoodMusic()
        self.weather_servicer = weather_servicer


    def register_playlist(self, request, context):
        """mood music passdown method handler for registering playlists
        Args:
            request (Request Obj) : Request object created with Proto protocall
            context (Context Obj) : Context object created with Proto protocall
        """

        try:
            # Attempt to use mood music register function 
            self.mood_music.register_playlist(request.user_id, request.title, request.mood, request.private)

            # Return formatted response
            return mood_music_service_pb2.register_playlist_response(message=f"Playlist with title {request.title} has been created successfully! Mood: {request.mood}")
        except Exception as e:
            # Return formatted error response
            print(f"An exception occurred: {e}")
            return mood_music_service_pb2.register_playlist_response(message=f"Something went wrong while registering playlist!")


    def register_user(self, request, context):
        """mood music passdown method handler for registering users
        Args:
            request (Request Obj) : Request object created with Proto protocall
            context (Context Obj) : Context object created with Proto protocall
        """
        try:
            # Attempt to use mood music register funciton
            self.mood_music.register_user(request.name, request.username, request.email, request.password)

            # Return formatted response
            return mood_music_service_pb2.register_user_response(message=f"Dear {request.name}, welcome! Your username: {request.username} with email: {request.email} registered successfully")
        except Exception as e:
            # Return formatted error repsonse
            print(f"An exception occurred: {e}")
            return mood_music_service_pb2.register_user_response(message=f"Something went wrong while registering user!")


    def register_song(self, request, context):
        """mood music passdown method handler for registering songs
        Args:
            request (Request Obj) : Request object created with Proto protocall
            context (Context Obj) : Context object created with Proto protocall
        """
        try:
            # Attempt to use mood music register function
            self.mood_music.register_song(request.title, request.artist, request.mood, request.lyrics)

            # Return formatted response
            return mood_music_service_pb2.register_song_response(message=f"Song with title {request.title} has been created successfully!")
        except Exception as e:
            # Return formatted error response
            print(f"An exception occurred: {e}")
            return mood_music_service_pb2.register_song_response(message=f"Something went wrong while registering song!")


    def register_timeline(self, request, context):
        """mood music passdown method for registering timelines
        Args:
            request (Request Obj) : Request object created with Proto protocall
            context (Context Obj) : Context object created with Proto protocall
        """
        try:
            # Attempt to use mood music register function
            self.mood_music.register_timeline(request.user_id)

            # Return formatted response
            return mood_music_service_pb2.register_timeline_response(message=f"Timeline with user_id {request.user_id} has been created successfully!")
        except Exception as e:
            # Return formatted error response
            print(f"An exception occurred: {e}")
            return mood_music_service_pb2.register_timeline_response(message=f"Something went wrong while registering timeline!")


    def delete_playlist(self, request, context):
        """mood music passdown method for deleting playlists
        Args:
            request (Request Obj) : Request object created with Proto protocall
            context (Context Obj) : Context object created with Proto protocall

        """
        try:
            # Attempt to use mood music delete function
            self.mood_music.delete_playlist(request.playlist_id)

            # Return formatted response        
            return mood_music_service_pb2.delete_playlist_response(message=f"Playlist: {request.playlist_id} deleted successfully!")
        except Exception as e:
            # Return formatted error response
            print(f"An exception occurred: {e}")
            return mood_music_service_pb2.delete_playlist_response(message=f"Something went wrong while deleting playlist!")


    def delete_user(self, request, context):
        """mood music passdown method for deleting users
        Args:
            request (Request Obj) : Request object created with Proto protocall
            context (Context Obj) : Context object created with Proto protocall
        """
        try:
            # Attempt to use mood music delete function
            self.mood_music.delete_user(request.user_id)

            # Return formatted response
            return mood_music_service_pb2.delete_user_response(message=f"User: {request.user_id} deleted successfully!")
        except Exception as e:
            # Return formatted error response
            print(f"An exception occurred: {e}")
            return mood_music_service_pb2.delete_user_response(message=f"Something went wrong while deleting user!")


    def share_playlist(self, request, context):
        """mood music passdown method for sharing playlists
        Args:
            request (Request Obj) : Request object created with Proto protocall
            context (Context Obj) : Context object created with Proto protocall
        """
        try:
            # Attempt to use mood music share function
            self.mood_music.share_playlist(request.user_id, request.playlist_id)

            # Return formatted response
            return mood_music_service_pb2.share_playlist_response(message=f"Here is your sharable link: https://moodmusic/playlist/{request.playlist_id}.")
        except Exception as e:
            # Return formatted error response
            print(f"An exception occurred: {e}")
            return mood_music_service_pb2.share_playlist_response(message=f"Something went wrong while sharing playlist!")


    def recommend_playlist(self, request, context):
        """mood music passdown method for recommending playlists
        Args:
            request (Request Obj) : Request object created with Proto protocall
            context (Context Obj) : Context object created with Proto protocall
        """
        try:
            # Find username
            # username = str(self.mood_music.users[request.user_id])

            # Attempt to use mood music reccomend function
            new_playlist_and_song_titles = self.mood_music.recommend_playlist(request.user_id, request.mood, request.title)
            song_titles = new_playlist_and_song_titles["songs"]

            # Generate response
            gen_message = f"{request.user_id}, we thought you'd like this playlist: {request.title} based on your mood: {request.mood}. We hope you'll enjoy it.\n"
            for title in song_titles:
                gen_message += f"{title}\n"

            # Return formatted response
            return mood_music_service_pb2.recommend_playlist_response(message = gen_message)

        except Exception as e:
            # Return formatted error response
            print(f"An exception occurred: {e}")
            return mood_music_service_pb2.recommend_playlist_response(message=f"Something went wrong while finding reccomendation!")


    def view_timeline(self, request, context):
        """mood music passdown method for viewing timelines of users
        Args:
            request (Request Obj) : Request object created with Proto protocall
            context (Context Obj) : Context object created with Proto protocall
        """
        try:
            # Attempt to use mood music view timeline function
            timeline = self.mood_music.view_timeline(request.user_id)

            # Return formatted response
            return mood_music_service_pb2.view_timeline_response(message=f'User: {request.user_id}\'s timeline: \n {timeline}')
        except Exception as e:
            # Return formatted error response
            print(f"An exception occurred: {e}")
            return mood_music_service_pb2.view_timeline_response(message=f'Something went wrong while trying output the timeline')


    def view_playlist(self, request, context):
        """mood music passdown method for viewing playlist info
        Args:
            request (Request Obj) : Request object created with Proto protocall
            context (Context Obj) : Context object created with Proto protocall
        """
        try:
            # Attempt to use mood music view timeline function
            playlist = self.mood_music.view_playlist(request.playlist_id)

            # Return formatted response
            return mood_music_service_pb2.view_playlist_response(message=f'playlist with id {request.playlist_id}\'s overview is \n{playlist}')
        except Exception as e:
            # Return formatted error response
            print(f"An exception occurred: {e}")
            return mood_music_service_pb2.view_playlist_response(message=f'Something went wrong while trying to view playlist information!')


    def view_song(self, request, context):
        """mood music passdown method for viewing songs info
        Args:
            request (Request Obj) : Request object created with Proto protocall
            context (Context Obj) : Context object created with Proto protocall
        """
        try:
            # Attempt to use mood music view timeline function
            song = self.mood_music.view_song(request.song_id)

            # Return formatted response
            return mood_music_service_pb2.view_song_response(message=f'Song with id {request.song_id}\' overview is \n {song}\n')
        except Exception as e:
            # Return formatted error response
            print(f"An exception occurred: {e}")
            return mood_music_service_pb2.view_song_response(message=f'Something went wrong while trying view song information!')


    def view_user(self, request, context):
        """mood music passdown method for viewing users info
        Args:
            request (Request Obj) : Request object created with Proto protocall
            context (Context Obj) : Context object created with Proto protocall
        """
        try:
            # Attempt to use mood music view timeline function
            user = self.mood_music.view_user(request.user_id)

            # Return formatted response
            return mood_music_service_pb2.view_user_response(message=f'User with id {request.user_id}\'s overview is {user}')
        except Exception as e:
            # Return formatted error response
            print(f"An exception occurred: {e}")
            return mood_music_service_pb2.view_user_response(message=f'Something went wrong while trying view user information!')


    def change_mood(self, request, context):
        """mood music passdown method for changing mood
        Args:
            request (Request Obj) : Request object created with Proto protocall
            context (Context Obj) : Context object created with Proto protocall
        """
        try:
            # Attempt to use mood music change mood function
            self.mood_music.change_mood(request.user_id, request.mood)

            # Return formatted response
            return mood_music_service_pb2.change_mood_response(message=f'User: {request.user_id}\'s mood has been changed successfully.')
        except Exception as e:
            # Return formatted error response
            print(f"An exception occurred: {e}")
            return mood_music_service_pb2.change_mood_response(message=f'Something went wrong while trying change users mood!')


    def log_mood(self, request, context):
        """mood music passdown method for logging mood
        Args:
            request (Request Obj) : Request object created with Proto protocall
            context (Context Obj) : Context object created with Proto protocall
        """
        current_weather = self.weather_servicer.get_current_weather()

        # Error checking and filtering data
        if(current_weather != None):

            # Set all values to their respected request information
            temp = current_weather.temp
            humidity = current_weather.humidity
            clouds = current_weather.clouds
            wind = current_weather.wind

        else:
            # Set all variables to empty incase of error somewhere down the chain
            temp = float()
            humidity = int()
            clouds = int()
            wind = float()

        try:
            # Attempt to use mood music log function
            self.mood_music.log_mood(request.user_id, request.song_id, temp, humidity, clouds, wind)

            # Return formatted response
            return mood_music_service_pb2.log_mood_response(message=f'User: {request.user_id}\'s mood has been logged successfully')
        except Exception as e:
            # Return formatted error response
            print(f"An exception occurred: {e}")
            return mood_music_service_pb2.log_mood_response(message=f"Something went wrong while logging weather and mood to your timeline!")  


    def create_mood_based_playlist(self, request, context):
        """
            Creates a mood-based playlist for a user.

            This method takes a user's ID and their current mood to generate a personalized
            playlist. It then communicates the success of this operation back to the client.

            Args:
                request (CreateMoodBasedPlaylistRequest): A request object that contains the user_id and mood.
                context (ServicerContext): The context object provided by gRPC for this call.

            Returns:
                CreateMoodBasedPlaylistResponse: A response object that includes a message
                indicating the success or failure of the playlist creation.
        """
        try:
            # Attempt to use mood music create mood based playlist function
            self.mood_music.create_mood_based_playlist(request.user_id, request.mood)

            # Return formatted message
            return mood_music_service_pb2.create_mood_based_playlist_response(message=f"{request.user_id}, your playlist has been created based on the mood: {request.mood}")
        except Exception as e:
            # Return formatted error message
            print(f"An exception occurred: {e}\nin create_mood_based_playlist")
            return mood_music_service_pb2.create_mood_based_playlist_response(message=f"Something went wrong while creating the playlist {request.mood}, please try again!")


    def add_song_to_playlist(self, request, context):
        """
            Adds a song to an existing playlist.

            This method takes a playlist ID and a song ID from the request and attempts to
            add the specified song to the specified playlist. It returns a message indicating
            the outcome of this operation.

            Args:
                request (AddSongToPlaylistRequest): A request object that contains the playlist_id and song_id.
                context (ServicerContext): The context object provided by gRPC for this call.

            Returns:
                AddSongToPlaylistResponse: A response object that includes a message indicating
                the success or failure of adding the song to the playlist.
    """
        try:
            # Attempt to use mood music add song to playlist function
            self.mood_music.add_song_to_playlist(request.playlist_id, request.song_id)

            # Return formatted message
            return mood_music_service_pb2.add_song_to_playlist_response(message=f"The song {request.song_id} has been added to the playlist {request.playlist_id} ")
        except Exception as e:
            # Return formatted error message
            print(f"An exception occurred: {e}\n in add_song_to_playlist")
            return mood_music_service_pb2.add_song_to_playlist_response(message=f"Could not add song to the playlist {request.playlist_id}, please try again!")


def server():
    """Main function of the program"""
    # Set Current working directory to project dir
    os.chdir(project_dir)

    # Configure server threads
    server = grpc.server(ThreadPoolExecutor(max_workers=10))  # Use ThreadPoolExecutor from concurrent.futures

    #mood_log_pb2_grpc.add_MoodLogServiceServicer_to_server(MoodLogServicer(), server)
    mood_music_service_pb2_grpc.add_MoodMusicServiceServicer_to_server(MoodMusicServicer(weather_servicer=WeatherServicer()), server)

    # Configure server to port 50051
    server.add_insecure_port('[::]:50051')

    # Start server
    server.start()
    print("Server started. Listening on port 50051.")
    server.wait_for_termination()
    

if __name__ == '__main__':
    server()

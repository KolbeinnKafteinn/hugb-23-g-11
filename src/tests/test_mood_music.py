import unittest
import sys
import os
project_root = os.path.abspath(os.path.join(os.path.dirname(__file__), '..'))
sys.path.insert(0, project_root)
from classes.mood_music import MoodMusic

class TestMoodMusic(unittest.TestCase):
    """The tests are made with the assistance of chatgpt."""
    def setUp(self):
        self.mood_music = MoodMusic()

    def tearDown(self):
        self.mood_music.reset()

    def test_register_user(self):
        """
        Test the register_user method
        You can create a user and then check if it exists in the users dictionary
        """
        self.mood_music.register_user(name="Test User", username="testuser", email="test@example.com", password="testpass")
        self.assertTrue("1" in self.mood_music.users)

    def test_register_playlist(self):
        """
        Test the register_playlist method
        You can create a playlist and then check if it exists in the playlists dictionary
        """
        self.mood_music.register_user(name="Test User", username="testuser", email="test@example.com", password="testpass")
        self.mood_music.register_playlist(user_id="1", title="Test Playlist", mood="Happy", private=False)
        self.assertTrue("1" in self.mood_music.playlists)

    def test_register_song(self):
        """
        Test the register_song method
        You can create a song and then check if it exists in the songs dictionary
        """
        self.mood_music.register_song(title="Test Song", artist="Test Artist", mood="Sad", lyrics="Some lyrics")
        self.assertTrue("1" in self.mood_music.songs)

    def test_delete_playlist(self):
        """
        Test the delete_playlist method
        Create a user and playlist, then delete the playlist and check if it's removed
        """
        self.mood_music.register_user(name="Test User", username="testuser", email="test@example.com", password="testpass")
        self.mood_music.register_playlist(user_id="1", title="Test Playlist", mood="Happy", private=False)
        self.assertTrue("1" in self.mood_music.playlists)
        self.mood_music.delete_playlist("Test Playlist")
        self.assertFalse("1" in self.mood_music.playlists)

    def test_add_remove_song_from_playlist(self):
        """
        Test the add_song_to_playlist and remove_song_from_playlist methods
        Create a user, playlist, and song, add the song to the playlist, then remove it
        """
        self.mood_music.register_user(name="Test User", username="testuser", email="test@example.com", password="testpass")
        self.mood_music.register_playlist(user_id="1", title="Test Playlist", mood="Happy", private=False)
        self.mood_music.register_song(title="Test Song", artist="Test Artist", mood="Sad", lyrics="Some lyrics")

        self.mood_music.add_song_to_playlist("Test Playlist", "Test Song")
        playlist = self.mood_music.playlists["1"]
        self.assertTrue(any("1" in song.id for song in playlist.songs))

        self.mood_music.remove_song_from_playlist("1", "1")
        self.assertFalse(any("1" in song.id for song in playlist.songs))

if __name__ == '__main__':
    unittest.main()

import unittest
import sys
import os
project_root = os.path.abspath(os.path.join(os.path.dirname(__file__), '..'))
sys.path.insert(0, project_root)
from classes.mood_music import MoodMusic

"""ChatGPT helped out with the creation of this test code"""

class TestSharePlaylist(unittest.TestCase):
    def setUp(self):
        # Initialize the MoodMusic instance for testing
        self.music_app = MoodMusic()
        
    def tearDown(self):
        self.music_app.reset()

    def test_share_playlist_valid(self):
        """
        Test sharing a playlist with a valid user in the music app.
        """

        # Create a user and a playlist
        user_id = "1"
        playlist_id = "1"
        self.music_app.register_user(name="User1", username="user1", email="user1@example.com", password="password")
        self.music_app.register_playlist(user_id, title="Playlist 1", mood="Happy", private=False)

        # Share the playlist and get the shareable link
        shareable_link = self.music_app.share_playlist(user_id, playlist_id)

        # Check that the shareable link is not None
        self.assertIsNotNone(shareable_link)

    def test_share_playlist_invalid_user(self):
        """
        Test sharing a playlist with an invalid user in the music app.
        """
        # Try to share a playlist with an invalid user
        user_id = "999"
        playlist_id = "1"

        # Attempt to share the playlist

        # Check that the shareable link is None (user not found)
        with self.assertRaises(Exception) as context:
            shareable_link = self.music_app.share_playlist(user_id, playlist_id)

        self.assertTrue("Invalid argument: Invalid user ID in share_playlist()", context.exception)

    def test_share_playlist_private_playlist(self):
        """
        Test sharing a private playlist in the music app.
        """
        # Create a user and a private playlist
        user_id = "1"
        playlist_id = "1"
        self.music_app.register_user(name="User1", username="user1", email="user1@example.com", password="password")
        self.music_app.register_playlist(user_id, title="Private Playlist", mood="Sad", private=True)

        # Try to share the private playlist
        shareable_link = self.music_app.share_playlist(user_id, playlist_id)

        # Check that the shareable link is None (cannot share private playlist)
        self.assertIsNone(shareable_link)

    def test_read_shared_playlist_valid(self):
        """
        Test reading a shared playlist in the music app.
        """
        # Create a user and a playlist
        user_id = "1"
        playlist_id = "1"
        self.music_app.register_user(name="User1", username="user1", email="user1@example.com", password="password")
        self.music_app.register_playlist(user_id, title="Playlist 1", mood="Happy", private=False)

        # Share the playlist and get the shareable link
        shareable_link = self.music_app.share_playlist(user_id, playlist_id)

        # Read the shared playlist
        shared_playlist = self.music_app.read_shared_playlist(shareable_link)

        # Check that the shared playlist is not None
        self.assertIsNotNone(shared_playlist)

if __name__ == '__main__':
    unittest.main()
import unittest
import sys
import os
project_root = os.path.abspath(os.path.join(os.path.dirname(__file__), '..'))
sys.path.insert(0, project_root)
from classes.mood_music import MoodMusic

class TestMoodMusic(unittest.TestCase):
    def setUp(self):
        self.mood_music = MoodMusic()

    def test_create_mood_based_playlist(self):
        user_id = "1"
        mood = "Happy"
        title = "Happy Life"
        
        # Register a user for testing
        self.mood_music.register_user("Test User", "testuser", "test@example.com", "password")
        # Create a mood-based playlist
        playlist = self.mood_music.register_playlist(user_id, title, mood, True)
        # Ensure the playlist was created
        self.assertTrue(playlist)
        # Check if the playlist is of the expected mood
        self.assertEqual(playlist.mood, mood)
        # Check if the title is the same
        self.assertEqual(playlist.title, title)
        # Check if the playlist contains songs with the same mood
        for song_id in playlist.songs:
            song = self.mood_music.songs[song_id]
            self.assertEqual(song.mood.lower(), mood.lower())

if __name__ == '__main__':
    unittest.main()

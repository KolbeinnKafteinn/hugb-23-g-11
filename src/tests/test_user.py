import unittest
import sys
import os
project_dir = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))
sys.path.append(project_dir)
from classes.user import User

class TestAlbum(unittest.TestCase):
    
    def test_create_user(self):
        """
        Test the creation of a user instance and its attributes.
        """

        user = User("Name", "Nobody78",  "Norway", "somebody@mail.is", "0", [])
        self.assertEqual(user.name, "Name")
        self.assertEqual(user.username, "Nobody78")
        self.assertEqual(user.email, "somebody@mail.is")
        self.assertEqual(user.password, "Norway")
        self.assertIsNotNone(user.id, "0")

if __name__ == '__main__':
    unittest.main()
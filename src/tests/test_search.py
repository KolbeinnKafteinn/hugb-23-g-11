import unittest
import sys
import os
project_root = os.path.abspath(os.path.join(os.path.dirname(__file__), '..'))
sys.path.insert(0, project_root)
from classes.mood_music import MoodMusic
from classes.song import Song  # Import the Song class
from classes.playlist import Playlist

class TestSearch(unittest.TestCase):    
    def setUp(self):
        self.mood_music = MoodMusic()
    
    def tearDown(self) -> None:
        self.mood_music.reset()
    
    def test_search_song(self):
        """
        Test the functionality of searching for a song in the MoodMusic class.
        """
        
        #Create 3 songs with slightly different names
        self.mood_music.register_song(title="song a", artist="Test Artist", mood="Sad", lyrics="Some lyrics")
        self.mood_music.register_song(title="song aa", artist="Test Artist", mood="Mad", lyrics="Some lyrics")
        self.mood_music.register_song(title="song b", artist="Test Artist", mood="Happy", lyrics="Some lyrics")
        
        #Checking if searching for "a" returns 2 songs
        result = self.mood_music.search_song("a")
        self.assertEqual(len(result), 2)
        self.assertEqual(result[0], "song a")
        self.assertEqual(result[1], "song aa")
        
        #Checking if searching for "b" returns 1 song
        result = self.mood_music.search_song("b")
        self.assertEqual(len(result), 1)
        self.assertEqual(result[0], "song b")
        
        #Checking if searching for "song" returns all of the songs
        result = self.mood_music.search_song("song")
        self.assertEqual(len(result), 3)
        self.assertEqual(result[0], "song a")
        self.assertEqual(result[1], "song aa")
        self.assertEqual(result[2], "song b")
    
    def test_search_user_playlist(self):
        """
        Test the functionality of searching for a user's playlist in the MoodMusic class.
        """
        
        #Create 2 users and 3 playlists, the first user made the first 2 playlists
        self.mood_music.register_user(name="Test User1", username="testuser1", email="test1@example.com", password="testpass1")
        self.mood_music.register_user(name="Test User2", username="testuser2", email="test2@example.com", password="testpass2")
        self.mood_music.register_playlist(user_id="1", title="Test Playlist11", mood="Sad", private=False)
        self.mood_music.register_playlist(user_id="1", title="Test Playlist12", mood="Happy", private=False)
        self.mood_music.register_playlist(user_id="2", title="Test Playlist21", mood="Mad", private=False)

        result = self.mood_music.search_user_playlist("Test", "1")
        self.assertEqual(len(result), 2)
        self.assertEqual(result[0].title, "Test Playlist11")
        self.assertEqual(result[1].title, "Test Playlist12")

        result = self.mood_music.search_user_playlist("11", "1")
        self.assertEqual(len(result), 1)
        self.assertEqual(result[0].title, "Test Playlist11")

        result = self.mood_music.search_user_playlist("21", "2")
        self.assertEqual(len(result), 1)
        self.assertEqual(result[0].title, "Test Playlist21")
        
        
        
        

if __name__ == "__main__":
    unittest.main()

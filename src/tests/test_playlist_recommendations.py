import unittest
import sys
import os
project_root = os.path.abspath(os.path.join(os.path.dirname(__file__), '..'))
sys.path.insert(0, project_root)
from classes.mood_music import MoodMusic
from classes.playlist import Playlist

class TestRecommendPlaylist(unittest.TestCase):
    def setUp(self):
        """ 
        Create an instance of YourPlaylistClass and initialize any necessary data
        """
        self.music_app = MoodMusic()
        
    def tearDown(self):
        self.music_app.reset()

    def test_recommend_playlist(self):
        """
        Test for recommending a playlist for a user with a specified mood.
        """
        user_id = "1"
        mood = "happy"
        title = "Happy Tunes"
        playlist_length = 5
        self.music_app.register_user(name="User123", username="user1", email="user1@example.com", password="password")

        #Create 5 songs that should all be added
        self.music_app.register_song(title="Test Song1", artist="Test Artist", mood="happy", lyrics="Some lyrics")
        self.music_app.register_song(title="Test Song2", artist="Test Artist", mood="happy", lyrics="Some lyrics")
        self.music_app.register_song(title="Test Song3", artist="Test Artist", mood="happy", lyrics="Some lyrics")
        self.music_app.register_song(title="Test Song4", artist="Test Artist", mood="happy", lyrics="Some lyrics")
        self.music_app.register_song(title="Test Song5", artist="Test Artist", mood="happy", lyrics="Some lyrics")
        
        # Call the method under test
        self.music_app.recommend_playlist("user1", mood, title, playlist_length)
        
        # Add assertions to verify the expected behavior
        # You may need to implement additional methods or accessors to retrieve data
        # from your YourPlaylistClass instance for verification.
        user = self.music_app.users[user_id]
        playlists = user.playlists
        self.assertEqual(playlists[0].title, title)
        self.assertEqual(len(playlists[0].songs), playlist_length)
        self.assertEqual(playlists[0].mood, mood)

    def test_recommend_playlist_based_on_few_moods(self):
        """
        Test for recommending a playlist for a user with a list of specified moods.
        """
        user_id = "1"
        mood_list = ["happy", "sad", "happy"]
        title = "Mixed tunes"
        playlist_length = 3
        self.music_app.register_user(name="User12", username="user12", email="user12@example.com", password="password12")

        #Create 5 songs that should all be added
        self.music_app.register_song(title="Test Song1", artist="Test Artist", mood="sad", lyrics="Some lyrics")
        self.music_app.register_song(title="Test Song2", artist="Test Artist2", mood="happy", lyrics="Some lyrics")
        self.music_app.register_song(title="Test Song3", artist="Test Artist", mood="sad", lyrics="Some lyrics")
        self.music_app.register_song(title="Test Song4", artist="Test Artist3", mood="happy", lyrics="Some lyrics")

        # Call the method under test
        self.music_app.recommend_playlist_based_on_few_moods(user_id, mood_list, title, playlist_length)

        # Add assertions to verify the expected behavior
        # You may need to implement additional methods or accessors to retrieve data
        # from your YourPlaylistClass instance for verification.
        user = self.music_app.users[user_id]
        playlists = user.playlists
        self.assertEqual(playlists[0].title, title)
        self.assertEqual(len(playlists[0].songs), playlist_length)
        self.assertEqual(playlists[0].mood, "mixed")


if __name__ == '__main__':
    unittest.main()
import unittest
import sys
import os
project_dir = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))
sys.path.append(project_dir)
from classes.song import Song

class TestSong(unittest.TestCase):
    
    def setUp(self):
        # Initialize a sample song for testing.
        self.song = Song(
            title="Test Song",
            artist="Test Artist",
            mood="Happy",
            id="12345",
            lyrics="These are the lyrics"
        )

    def test_create_song(self):
        """
        Test the creation of a song instance and its attributes.
        """
        self.assertEqual(self.song.title, "Test Song")
        self.assertEqual(self.song.artist, "Test Artist")
        self.assertEqual(self.song.mood, "Happy")
        self.assertEqual(self.song.id, "12345")
        self.assertEqual(self.song.lyrics, "These are the lyrics")

    def test_str_method(self):
        """
        Test the string representation method of the Song class.
        """
        expected_str = "Test Artist - Test Song"
        self.assertEqual(str(self.song), expected_str)

    def test_json_method(self):
        """
        Test the JSON representation method of the Song class.
        """
        expected_json = {
            "artist": "Test Artist",
            "lyrics": "These are the lyrics",
            "title": "Test Song",
            "mood": "Happy",
            "id": "12345"
        }
        self.assertEqual(self.song.json(), expected_json)

if __name__ == '__main__':
    unittest.main()
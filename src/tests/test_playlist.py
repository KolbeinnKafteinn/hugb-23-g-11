import unittest
import sys
import os
project_root = os.path.abspath(os.path.join(os.path.dirname(__file__), '..'))
sys.path.insert(0, project_root)
from classes.playlist import Playlist


class TestPlaylist(unittest.TestCase):
    
    def setUp(self):
        # Initialize a sample playlist for testing.
        self.playlist = Playlist(author="John", title="Test Playlist", mood="Happy", id="12345", songs=list())

    def test_create_playlist(self):
        """
        Test the creation of a playlist instance and its attributes.
        """
        self.assertEqual(self.playlist.author, "John")
        self.assertEqual(self.playlist.title, "Test Playlist")
        self.assertEqual(self.playlist.mood, "Happy")
        self.assertEqual(self.playlist.id, "12345")
        self.assertTrue(self.playlist.private)
        self.assertEqual(self.playlist.songs, [])

if __name__ == '__main__':
    unittest.main()
import unittest
import json
from datetime import datetime
import sys
import os
project_dir = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))
sys.path.append(project_dir)
from classes.timeline import Timeline

class TestTimeline(unittest.TestCase):
    """The tests are made with the assistance of chatgpt."""
    def setUp(self):
        self.timeline = Timeline("1","1")

    def test_log(self):
        """
        Test the logging function in the Timeline class.
        """
        mood = "happy"
        song_id = "1"
        self.timeline.log(mood, song_id, '20', '84', '100', '14', '20')
        date, time = self.timeline.get_datetime()
        self.assertEqual(self.timeline.timeline[date][time], [mood, song_id, '20', '84', '100', '14', '20'])

    def test_get_datetime(self):
        """
        Test the retrieval of the current date and time from the Timeline class.
        """
        date, time = self.timeline.get_datetime()
        self.assertIsNotNone(date)
        self.assertIsNotNone(time)

    def test_str(self):
        """
        Test the string representation method of the Timeline class.
        """
        # REWRITE FOR NEW STR METHOD
        timeline_str = str(self.timeline)

        try:
            self.assertTrue(isinstance(timeline_str, str))
        except:
            self.fail("Failed to assert is str")


if __name__ == '__main__':
    unittest.main()
class User:
    def __init__(self, name:str,username:str, password:str, email:str, id:str, playlists:list, mood:str="", mood_timeline:any = "") -> None:
        self.mood_timeline = mood_timeline
        self.playlists = playlists
        self.username = username
        self.password = password
        self.email = email
        self.mood = mood
        self.name = name
        self.id = id

    def __str__(self) -> str:
        """
        Returns a string representation of object
        """
        return f"{self.username}#{self.id}"

    def create_playlist(self, playlist:any) -> None:
        """
        Create a playlist for user
        
        Args:
            playlist (Playlist Object): playlist object to be added 
        """
        # Append item to self.playlist
        self.playlists.append(playlist)

    def change_mood(self, mood:str) -> None:
        """
        Changes users mood

        Args:
            mood (str) : mood to be changed
        """

        # Set users' mood
        self.mood = mood

    def delete_playlist(self, playlist:any) -> None:
        """
        Delete a playlist by user

        Args:
            playlist (Playlist Object): playlist object to be removed
        """
        # Remove playlist from self.playlist
        self.playlists.remove(playlist)

    def log_mood(self, song_id:str, temp:str, humitidy:str, clouds:str, wind:str) -> None:
        """
        Pass down method of logging mood
        
        Args:
            song_id (str) : id of song user was playing while logged
            weather (str) : dictionary representaion of current users' weather
        """
        self.mood_timeline.log(self.mood, song_id, temp, humitidy, clouds, wind)

    def json(self) -> dict:
        """
        Returns a json representation of object
        """
        return {
            "playlists" : [x.id for x in self.playlists],
            "mood_timeline" : self.mood_timeline.json(),
            "username" : self.username,
            "password" : self.password,
            "email": self.email,
            "mood" : self.mood,
            "name" : self.name,
            "id" : self.id
        }

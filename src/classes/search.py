from classes.song import Song
from classes.playlist import Playlist

class Search:
    def __init__(self, songs = None, playlists = None):
        self.songs = songs
        self.playlist = playlists

    def search_by_title(self, title):
        """
        Search for songs by title and return matching songs.
        
        Args:
            title (str): The title to search for.

        Returns:
            list: A list of matching songs.
        """
        matching_songs = []
        for song_id, song in self.songs.items(): # Iterate through the songs in the collection.
            if title.lower() in song.title.lower(): # Check if the provided title is a substring of the song's title.
                matching_songs.append(song) # If it's a match, add the song to the list of matching songs.
        return matching_songs

    def search_by_artist(self, artist):
        """
        Search for songs by artist and return matching songs.
        
        Args:
            artist (str): The artist to search for.

        Returns:
            list: A list of matching songs.
        """
        matching_songs = []
        for song_id, song in self.songs.items(): # Iterate through the songs in the collection.
            if artist.lower() in song.artist.lower(): # Check if the provided artist is a substring of the song's artist.
                matching_songs.append(song) # If it's a match, add the song to the list of matching songs.
        return matching_songs
    
    def search_by_playlist_name(self, title):
        """
        Search for playlists by name and return matching playlists.
        
        Args:
            title (str): The name to search for.

        Returns:
            list: A list of matching playlists.
        """
        matching_playlist = []
        for playlist_id, playlist in self.playlist.items(): # Iterate through the playlists in the collection.
            if title.lower() in playlist.title.lower(): # Check if the provided title (case-insensitive) is a substring of the playlist's title.
                matching_playlist.append(playlist) # If it's a match, add the playlist to the list of matching playlists.
        return matching_playlist

    def search_by_author(self, author):
        """
        Search for playlists by author and return matching playlists.

        Args:
            author (str): The author to search for.

        Returns:
            list: A list of matching playlists.
        """        
        matching_playlist = []
        for playlist_id, playlist in self.playlist.items(): # Iterate through the playlists in the collection.
            if author.lower() in playlist.author.lower(): # Check if the provided author (case-insensitive) is a substring of the playlist's author.
                matching_playlist.append(playlist) # If it's a match, add the playlist to the list of matching playlists.
        return matching_playlist

    def search_by_mood(self, mood):
        """
        Search for playlists by mood and return matching playlists.

        Args:
            mood (str): The mood to search for.

        Returns:
            list: A list of matching playlists.
        """        
        matching_playlist = []
        for playlist_id, playlist in self.playlist.items():  # Iterate through the playlists in the collection.
            if mood.lower() in playlist.mood.lower(): # Check if the provided mood (case-insensitive) is a substring of the playlist's mood.
                matching_playlist.append(playlist) # If it's a match, add the playlist to the list of matching playlists.
        return matching_playlist
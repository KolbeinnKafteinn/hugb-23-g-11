from datetime import datetime
import json

class Timeline():
    """
    A class that helps with logging moods on a mood timeline
    Class variables are:
    1. a list of moods that can be selected
    2. a dictionary that keeps track of all of the moods logged by the user/system
    """

    def __init__(self, id:str, user_id:str, timeline:dict=dict()) -> None:
        self.id = id
        self.user_id = user_id
        self.timeline = timeline

        self.mood_list = ["happy", "sad", "mad"]

    def __str__(self) -> str:
        """
        returns a str representaion of object
        Returns:
            str: timeline in json format
        """

        entries = ""

        # Iterate through each date entry
        for date_key in [*self.timeline.keys()]:

            # Iterate through each time entry
            for time_key in [*self.timeline[date_key].keys()]:

                # format text for return later
                mood = self.timeline[date_key][time_key][0]
                song = self.timeline[date_key][time_key][1]
                temp = self.timeline[date_key][time_key][2]
                humitidy = self.timeline[date_key][time_key][3]
                clouds = self.timeline[date_key][time_key][4]
                wind = self.timeline[date_key][time_key][5]

                entries += "\n"
                entries += f'date : {date_key}\n'
                entries += f'time : {time_key}\n'
                entries += f'mood : {mood}\n'
                entries += f'song_id : {song}\n'
                entries += f'temp : {float(temp):.2f} °C\n'
                entries += f'humidity : {humitidy} %\n'
                entries += f'clouds : {clouds} %\n'
                entries += f'wind : {float(wind):.2f} km/h\n'

        #return json.dumps(self.timeline, indent=4)
        return entries

    def json(self) -> dict:
        """
        Returns a json representation of object
        """
        return {
            "id" : self.id,
            "user_id" : self.user_id,
            "timeline" : self.timeline
        }

    def log(self, mood:str, song:str, temp:str="", humitidy:str="", clouds:str="", wind:str="", wind_deg:str="") -> None:
        """
        logs a song and the mood associated with it to the timeline
        
        Args:
            mood (str): the mood selected
            song (str): the song selected
            temp (str) : current temperature
            humidity (str) : current humidity
            clouds (str) : current clouds
            wind (str) : current winds
            wind_degrees (str) : current wind degrees
        """
        # Get date and time
        date, time = self.get_datetime()

        # if date doesnt already exits, create entry of it
        if date not in self.timeline:
            self.timeline[date] = dict()

        # log mood and song at date, time 
        self.timeline[date][time] = [mood, song, temp, humitidy, clouds, wind, wind_deg]
        
    def prompt(self, song:str):
        """
        logs a song and mood based on what the user associates it with
        
        Args:
            song (str): the song selected
        """
        # Prompt user for mood
        print(f"Select a mood the song '{song}' made you feel")

        # Print out possilbe moods
        for mood_nr in range(len(self.mood_list)):
            print(f"{mood_nr+1}. {self.mood_list[mood_nr]}")

        # Prompr user for mood choice
        mood = int(input(f"> ").strip())

        # Log results into mood log
        self.log(self.mood_list[mood-1], song)

    def get_datetime(self):
        """
        Gets the current date and time so things can be logged correctly
        
        Returns:
            tuple: the current date and time
        """
        # Utilize dateteim library to fetch current time and date
        now = datetime.now()
        date = now.strftime("%d/%m/%Y")
        time = now.strftime("%H:%M")

        # return date, time tuple
        return date,time

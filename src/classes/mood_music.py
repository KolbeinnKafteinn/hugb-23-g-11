from classes.playlist import Playlist
from classes.timeline import Timeline
from classes.song import Song
from classes.user import User

# from classes.artist import Artist
# from classes.album import Album

import json
import random

class MoodMusic:
    def __init__(self) -> None:
        # Set constants for file srcs
        self.PLAYLISTS_SRC = "data/playlists.json"
        self.TIMELINES_SRC = "data/timeline.json"
        self.USERS_SRC = "data/users.json"
        self.SONGS_SRC = "data/songs.json"
        #self.ALBUMS_SRC = "data/albums.json"
        #self.ARTISTS_SRC = "data/artists.json"

        # Initialize important files
        self.get_songs()
        self.get_playlists()
        self.get_timelines()
        self.get_users()
        #self.get_albums()
        #self.get_artists()


    def reset(self) -> None:
        """
        Reset all self variables
        """
        self.users = {}
        self.user_ids = 0
        self.songs = {}
        self.song_ids = 0
        self.playlists = {}
        self.playlist_ids = 0
        self.timelines = {}
        self.timeline_ids = 0


    def _print_users(self) -> None:
        """
        Developer function to print all users in system
        """
        # Iterate through each key in dicationary as a list and print out value assocaited with key
        for key in [*self.users.keys()]:
            print(self.users[key])


    def _print_playlists(self) -> None:
        """
        Developer function to print all playlists in system
        """
        # Iterate through each key in dicationary as a list and print out value assocaited with key
        for key in [*self.playlists.keys()]:
            print(self.playlists[key])


    def _print_songs(self) -> None:
        """
        Developer function to print all songs in system
        """
        # Iterate through each key in dicationary as a list and print out value assocaited with key
        for key in [*self.songs.keys()]:
            print(self.songs[key])


    def _print_timelines(self) -> None:
        """
        Developer function to print all timelines in system
        """
        # Iterate through each key in dicationary as a list and print out value assocaited with key
        for key in [*self.timelines.keys()]:
            print(self.timelines[key])


    def get_users(self) -> None:
        """
        Import users from USERS_SRC file
        """
        # Open files and import data from json
        try:
            users_data = open(self.USERS_SRC, "r")
            users_json = json.loads(users_data.read())
            users_data.close()
        except Exception as e:
            users_json = []
            raise Exception(f"An error occured: {e} \n in get_users()")

        # Setup nescessary files
        self.users = {}
        self.user_ids = 0

        # Iterate through each item and create user class from data
        for current_user in users_json:
            user = User(
                mood_timeline=self.timeline_ids["mood_timeline"],
                #mood_timeline=current_user["mood_timeline"], # Make reference to self.timeline
                playlists=[self.playlists[index] for index in current_user["playlists"]],
                username=current_user["username"],
                password=current_user["password"],
                email=current_user["email"],
                mood=current_user["mood"],
                name=current_user["name"],
                id=current_user["id"]
            )

            # Register user to dictionary
            self.users[current_user["id"]] = user
            self.user_ids += 1


    def get_playlists(self) -> None:
        """
        Import playlists from PLAYLIST_SRC file
        """
        # Open files and import data from json
        try:
            playlists_data = open(self.PLAYLISTS_SRC, "r")
            playlists_json = json.loads(playlists_data.read())
            playlists_data.close()
        except Exception as e:
            playlists_json = []
            raise Exception(f"An error occured: {e} \n in get_playlist()")


        # Setup nescessary files
        self.playlists = {}
        self.playlist_ids = 0

        # Iterate through each item and create playlist class from data
        for current_playlist in playlists_json:
            playlist = Playlist(
                private=current_playlist["private"],
                author=current_playlist["author"],
                songs=[self.songs[index] for index in current_playlist["songs"]],
                title=current_playlist["title"],
                mood=current_playlist["mood"],
                id=current_playlist["id"]
            )

            # Register playlist to playlists id
            self.playlists[current_playlist["id"]] = playlist 
            self.playlist_ids += 1


    def get_songs(self) -> None:
        """
        Import songs from SONGS_SRC file
        """
        # Open files and import data from json
        try:
            songs_data = open(self.SONGS_SRC, "r")
            songs_json = json.loads(songs_data.read())
            songs_data.close()
        except Exception as e:
            songs_json = []
            raise Exception(f"An error occured: {e} \n in get_songs()")

        # Setup nescessary files
        self.songs = {}
        self.song_ids = 0

        # Iterate through each item and create song class from data
        for current_song in songs_json:
            song = Song(
                artist=current_song["artist"],
                lyrics=current_song["lyrics"],
                title=current_song["title"],
                mood=current_song["mood"],
                id=current_song["id"]
            )

            # Register song to songs dictionary
            self.songs[current_song["id"]] = song
            self.song_ids += 1
    

    def get_timelines(self) -> None:
        """
        Import timelines from TIMELINES_SRC file
        """
        # Open files and import data from json
        try:
            timeline_data = open(self.TIMELINES_SRC, "r")
            timeline_json = json.loads(timeline_data.read())
            timeline_data.close()
        except Exception as e:
            timeline_json = []
            raise Exception(f"An error occured: {e} \n in get_timelines()")

        # Setup necessary files
        self.timelines = {}
        self.timeline_ids = 0

        # Iterate through each item and create timeline class from data
        for current_timeline in timeline_json:
            timeline = Timeline(
                timeline = json.loads(current_timeline['timeline']),
                user_id = current_timeline['user_id'],
                id = current_timeline['id']
            )

            self.timelines[current_timeline["id"]] = timeline
            self.timeline_ids += 1


    def save_users(self) -> None:
        """
        save all current catalogged users in USERS_SRC file
        """
        # Get all ids associated with users as list
        user_keys = [*self.users.keys()]

        # Get each users' json with given id
        users_json = [self.users[key].json() for key in user_keys]

        # Save json result
        try:
            file_handle = open(self.USERS_SRC, "w")
            file_handle.write(json.dumps(users_json))
            file_handle.close()
        except Exception as e:
            raise Exception(f"An error occured: {e} \n in save_users()")


    def save_playlists(self) -> None:
        """
        save all current catalogged playlists in PLAYLISTS_SRC file
        """
        # Get all ids associated with playlists as a list
        playlists_keys = [*self.playlists.keys()]

        # Get each playlists' json with given id
        playlists_json = [self.playlists[key].json() for key in playlists_keys]

        # Save json result
        try:
            file_handle = open(self.PLAYLISTS_SRC, "w")
            file_handle.write(json.dumps(playlists_json))
            file_handle.close()
        except Exception as e:
            raise Exception(f"An error occured: {e} \n in save_playlists()")


    def save_songs(self) -> None:
        """
        save all current catalogged songs in SONGS_SRC file
        """
        # Get all ids associated with songs as list
        songs_keys = [*self.songs.keys()]

        # Get each songs' json with given id
        songs_json = [self.songs[key].json() for key in songs_keys]

        # Save json results
        try:
            file_handle = open(self.SONGS_SRC, "w")
            file_handle.write(json.dumps(songs_json))
            file_handle.close()
        except Exception as e:
            raise Exception(f"An error occured: {e} \n in save_songs()")


    def save_timelines(self) -> None:
        """
        save all current catalogged timelines in TIMELINES_SRC file
        """
        # Get all ids associated with timeline as list
        timelines_keys = [*self.timelines.keys()]

        # Get each timelines' json with given id
        timelines_json = [self.timelines[key].json() for key in timelines_keys]

        # Save json results
        try:
            file_handle = open(self.TIMELINES_SRC, "w")
            file_handle.write(json.dumps(timelines_json))
            file_handle.close()
        except Exception as e:
            raise Exception(f"An error occured: {e} \n in save_timelines()")


    def register_user(self, name: str, username: str, email: str, password: str) -> None:
        """
        Register user with the given data provided by the user.

        Args:
            name (str): Name of the user.
            username (str): Username of the user.
            email (str): Email of the user.
            password (str): Password of the user.
        """
        try:
            if not (name and username and email and password):
                raise ValueError("Missing required argument(s)")

            # Generate a new user ID for the user
            id = str(self.user_ids + 1)

            # Add testing here for variables!!! TODO

            # Create user object
            user = User(
                name=name,
                username=username,
                email=email,
                password=password,
                id=id,
                playlists=[],
                mood_timeline=self.register_timeline(id)
            )

            # Add the user to the user list and update the available user ID
            self.users[id] = user
            self.user_ids += 1
        except ValueError as ve:
            raise Exception(f"Invalid argument: {ve}\n in register_user()")
        except Exception as e:
            raise Exception(f"An error occurred: {e}\n in register_user()")


    def register_timeline(self, user_id: str):
        """
        Create a new timeline given a user ID.

        Args:
            user_id (str): The ID of the user.

        Returns:
            timeline (Timeline): Timeline Object
        """
        try:
            if not user_id:
                raise ValueError("Missing required argument")

            # Generate a new timeline ID for the timeline
            id = str(self.timeline_ids + 1)

            # Create the timeline object
            timeline = Timeline(
                user_id=user_id,
                timeline=dict(),
                id=id,
            )

            # Add the timeline to the system storage and increment the available ID
            self.timelines[id] = timeline
            self.timeline_ids += 1

            return timeline
        except ValueError as ve:
            raise Exception(f"Invalid argument: {ve}\n in register_timeline()")
        except Exception as e:
            raise Exception(f"An error occurred: {e}\n in register_timeline()")


    def register_playlist(self, user_id: str, title: str, mood: str, private: bool) -> None:
        """
        Register a playlist given a user ID.

        Args:
            user_id (str): The ID of the user.
            title (str): The title of the playlist.
            mood (str): The mood of the playlist.
            private (bool): The status of the privacy of the playlist.
        """

        try:
            if not user_id or not title or not mood:
                raise ValueError("Missing required arguments")

            # Generate a new playlist ID for the playlist
            id = str(self.playlist_ids + 1)

            # Get the user associated with the user_id
            user = self.users[user_id]

            # Create the playlist object
            playlist = Playlist(
                author=user_id,
                title=title,
                mood=mood,
                id=id,
                songs=[],
                private=private
            )

            # Add the playlist to the user locally and globally, and increment the available playlist ID
            user.create_playlist(playlist)
            
            # Add playlist to registry
            self.playlists[id] = playlist
            
            # increment available id
            self.playlist_ids += 1

            return playlist

        except ValueError as ve:
            raise Exception(f"Invalid argument: {ve}\n in register_playlist()")
        except Exception as e:
            raise Exception(f"An error occurred: {e}\n in register_playlist()")


    def register_song(self, title: str, artist: str, mood: str, lyrics: str = "") -> None:
        """
        Registers a song given data provided by the user.

        Args:
            title (str): The title of the song.
            artist (str): The ID of the artist.
            mood (str): The mood of the song.
            lyrics (str): The lyrics of the song.
        """
        try:
            if not title or not artist or not mood:
                raise ValueError("Missing required arguments")

            # Generate a new song ID for the song
            id = str(self.song_ids + 1)

            # Create the song object
            song = Song(
                title=title,
                artist=artist,
                mood=mood,
                id=id,
                lyrics=lyrics
            )

            self.songs[id] = song
            self.song_ids += 1
        except ValueError as ve:
            raise Exception(f"Invalid argument: {ve}\n in register_song()")
        except Exception as e:
            raise Exception(f"An error occurred: {e}\n in register_song()")


    def delete_playlist(self, title: str) -> None:
        """
        Deletes an entire playlist from the user and the system.

        Args:
            title (str): The title of the playlist.
        """
        try:
            playlist_id = next((id for id, playlist in self.playlists.items() if playlist.title == title), None)
            if playlist_id not in self.playlists:
                raise ValueError("Invalid playlist ID")

            # Get playlist object from playlist dictionary
            playlist = self.playlists[playlist_id]

            # Get user id from playlist
            user_id = playlist.author

            # Get user object from user dictionary
            user = self.users[user_id]

            # Use the built-in remove_playlist method locally on the user level
            user.delete_playlist(playlist)

            # Remove the playlist on the system level
            del self.playlists[playlist_id]
            
        except ValueError as ve:
            raise Exception(f"Invalid argument: {ve}\n in delete_playlist()")
        except Exception as e:
            raise Exception(f"An error occurred: {e}\n in delete_playlist()")


    def delete_user(self, username: str) -> None:
        """
        Deletes a user from the system by username.

        Args:
            username (str): The username of the user.
        """
        try:
            # Find user_id by username
            user_id = next((id for id, user in self.users.items() if user.username == username), None)
            
            if user_id is None:
                raise ValueError("Invalid username")

            # Gets the user object and all its playlists
            user = self.users[user_id]
            playlists = user.playlists

            # Removes every playlist from the user object and moody registry
            for playlist in playlists:
                self.delete_playlist(playlist.id)

            # Remove the user from the registry
            del self.users[user_id]
        except ValueError as ve:
            raise Exception(f"Invalid argument: {ve}\n in delete_user_by_username()")
        except Exception as e:
            raise Exception(f"An error occurred: {e}\n in delete_user_by_username()")



    def add_song_to_playlist(self, playlist_title: str, song_title: str) -> None:
        """
        Adds a song to a given playlist by using their titles.

        Args:
            playlist_title (str): The title of the playlist.
            song_title (str): The title of the song.
        """
        try:
            # Find the playlist by title
            playlist = next((playlist for playlist in self.playlists.values() if playlist.title == playlist_title), None)
            if playlist is None:
                raise ValueError("Playlist with the given title not found")

            # Find the song by title
            song = next((song for song in self.songs.values() if song.title == song_title), None)
            if song is None:
                raise ValueError("Song with the given title not found")

            # Add the song to the playlist
            playlist.add(song)

        except ValueError as ve:
            raise ValueError(f"Invalid argument: {ve}\n in add_song_to_playlist()")
        except Exception as e:
            raise Exception(f'An error occurred: {e}\n in add_song_to_playlist()')




    def remove_song_from_playlist(self, playlist_id: str, song_id: str) -> None:
        """
        Removes a given song from a given playlist.

        Args:
            playlist_id (str): The ID of the playlist.
            song_id (str): The ID of the song.
        """
        try:
            if playlist_id not in self.playlists:
                raise ValueError("Invalid playlist ID")

            if song_id not in self.songs:
                raise ValueError("Invalid song ID")

            # Get the playlist object from the playlist dictionary
            playlist = self.playlists[playlist_id]

            # Get the song object from the song list
            song = self.songs[song_id]

            # Remove the song from the playlist
            playlist.remove(song)
        except ValueError as ve:
            raise Exception(f"Invalid argument: {ve}\n in remove_song_to_playlist()")
        except Exception as e:
            raise Exception(f"An error occurred: {e}\n in remove_song_to_playlist()")


    def search_song(self, input:str) -> list: 
        """
        searches for a given song/songs based on input and prints them out
        
        Args:
            input (str): the input the user searched for
        """
        try:
            # Create catalogging variable for the song search
            search = []

            # Iterate through each song in system
            for song in range(len(self.songs)):
                # If search term and title match
                if input.lower() in self.songs[str(song+1)].title.lower():
                    # Append item to catalog
                    search.append(self.songs[str(song+1)].title)
            
            # Return results
            return search
        
        except ValueError as ve:
            raise Exception(f"Invalid argument: {ve}\n in search_song()")
        except Exception as e:
            raise Exception(f"An error occurred: {e}\n in search_song()")

    def search_user_playlist(self, input_str: str, user_id: str) -> list:
        """
        Searches for a given playlist or playlists based on the input and returns the matching playlists.

        Args:
            input_str (str): The input the user searched for.
            user_id (str): The ID of the current user.

        Returns:
            list: A list of playlists that match the search input.
        """
        try:
            if user_id not in self.users:
                raise ValueError("Invalid user ID")

            # Gets the current user based on user_id
            user = self.users[user_id]

            # Get user playlists
            playlists = user.playlists

            # Create a cataloging variable for the song search
            search = []

            # Iterate through playlists
            for playlist in playlists:
                # If title matches search
                if input_str.lower() in playlist.title.lower():
                    # Append the item to the catalog
                    search.append(playlist)

            return search
        except ValueError as ve:
            raise Exception(f"Invalid argument: {ve}\n in search_user_in_playlist()")
        except Exception as e:
            raise Exception(f"An error occurred: {e}\n in search_user_in_playlist()")


    def share_playlist(self, user_id: str, playlist_id: str) -> str:
        """
        Share a playlist with a shareable link.

        Args:
            user_id (str): ID of the user sharing the playlist.
            playlist_id (str): ID of the playlist to be shared.

        Returns:
            str: The shareable link if sharing was successful, None otherwise.
        """
        try:
            if user_id not in self.users:
                raise ValueError("Invalid user ID")

            user = self.users[user_id]

            playlists = user.playlists

            for playlist in playlists:
                if playlist.id == playlist_id:
                    # Cannot share a private playlist
                    if playlist.private:
                        return None
                    # Generate a shareable link
                    shareable_link = f"https://moodmusic/playlist/{playlist_id}"
                    return shareable_link

            # Playlist not found
            return None
        except ValueError as ve:
            raise Exception(f"Invalid argument: {ve}\n in share_playlist()")
        except Exception as e:
            raise Exception(f"An error occurred: {e}\n in share_playlist()")


    def read_shared_playlist(self, shareable_link:str):
        """
        Read a shared playlist from a given shareable link and return the playlist3 object.
        
        Args:
            shareable_link (str): The shareable link to read.

        Returns:
            Playlist: The playlist object if found, None otherwise.
        """

        try:
            if not isinstance(shareable_link, str):
                raise ValueError('shareable_link must be a string')
            
            # Parse the playlist ID from the shareable link
            if shareable_link == None:
                return
            
            playlist_id = shareable_link.split("/")[-1]

            if playlist_id in self.playlists:
                return self.playlists[playlist_id]
            else:
                return None  # Playlist not found

        except ValueError as ve:
            raise Exception(f"Invalid argument: {ve}\n in read_shared_playlist()")
        except Exception as e:
            raise Exception(f"An error occurred: {e}\n in read_shared_playlist()")


    def recommend_playlist(self, username: str, mood: str, title="{mood}", playlist_length=10):
        """
        Makes a custom playlist for the user, based on the current mood.

        Args:
            username (str): USername of the current user.
            mood (str): Mood of the user.
            title (str, optional): Name for the playlist. Defaults to "{mood}".
            playlist_length (int, optional): Length of the playlist. Defaults to 10.

        Returns:
            Playlist: A playlist object made by mood.
        """
        try:
            user_id = next((id for id, user in self.users.items() if user.username == username), None)
            if user_id not in self.users:
                raise ValueError("Username does not exist") # Because the function takes
            if not isinstance(mood, str):
                raise ValueError("Mood must be a string")
            if not isinstance(title, str):
                raise ValueError("Title must be a string")
            if not isinstance(playlist_length, int) or playlist_length <= 0:
                raise ValueError("Playlist length must be a positive integer")

            # Register new playlist in system
            self.register_playlist(user_id, title, mood, False)

            # Get id of playlist
            playlist_id = self.playlist_ids

            # Keep counter to maintain the ammount of songs
            counter = 0

            # Keep track of title for songs added for other functions
            title_of_songs_list = []
            # Iterate through each song in system
            for song_id in self.songs:

                # If songs mood matches with mood parameter
                if(self.songs[song_id].mood.lower() == mood.lower()):
                    # check counter 
                    if(counter == playlist_length):
                        # Do nothing
                        break
                    else:
                        # Add song to playlist, increment counter
                        counter += 1
                        self.add_song_to_playlist(title, self.songs[song_id].title)
                        title_of_songs_list.append(self.songs[song_id].title)

            # Return generated playlist and titles of songs
            return_item = {"playlist": self.playlists[str(playlist_id)], "songs": title_of_songs_list}
            return return_item

        except ValueError as ve:
            raise Exception(f"Invalid argument: {ve}\n in recommend_playlist()")
        except Exception as e:
            raise Exception(f"An error occurred: {e}\n in recommend_playlist()")

    
    def log_mood(self, username: str, song_id: str, temp:float, humitidy:int, clouds:int, wind:float):
        """
        Passdown method of logging mood to the user.

        Args:
            username (str): USername of the current user
            song_id (str): ID of the song
            temp (float) : current temperature in celsius
            humidity (int) : current humidity in percentages
            clouds (int) : current clouds in percentages
            wind (float) : current winds in km/h
        """
        try:
            user_id = next((id for id, user in self.users.items() if user.username == username), None)
            if user_id not in self.users:
                raise ValueError("Invalid user ID")
            if song_id not in self.songs:
                raise ValueError("Invalid song ID")
            if not isinstance(temp, float):
                raise ValueError("temperature must be a float")
            if not isinstance(humitidy, int):
                raise ValueError("humitity must be a int")
            if not isinstance(clouds, int):
                raise ValueError("clouds must be a int")
            if not isinstance(wind, float):
                raise ValueError("wind must be a float")

            # Get user
            user = self.users[user_id]

            # Use the user method to log the mood
            user.log_mood(song_id, temp, humitidy, clouds, wind)

        except ValueError as ve:
            raise Exception(f"Invalid argument: {ve}\n in log_mood()")
        except Exception as e:
            raise Exception(f"An error occurred: {e}\n in log_mood()")


    def recommend_playlist_based_on_few_moods(self, user_id: str, mood_list: list, title: str = "MoodMix", playlist_length: int = 10):
        """
        Makes a custom playlist for the user, based on the current mood.

        Args:
            user_id (str): ID of the current user.
            mood_list (list): Three non-unique moods in a list from previous few moods.
            title (str, optional): Name for the playlist. Defaults to "{mood} {playlist_length}".
            playlist_length (int, optional): Length of the playlist. Defaults to 10.

        Returns:
            Playlist: A playlist object made by mood.
        """
        try:
            if not isinstance(user_id, str):
                raise ValueError("user_id must be a string")
            if not isinstance(mood_list, list) or len(mood_list) != 3:
                raise ValueError("mood_list must be a list containing three non-unique moods")
            if not isinstance(title, str):
                raise ValueError("title must be a string")
            if not isinstance(playlist_length, int) or playlist_length <= 0:
                raise ValueError("playlist_length must be a positive integer")

            # Make a new playlist to recommend
            self.register_playlist(user_id=user_id, title=title, mood='mixed', private=False)

            # Get playlist id
            playlist_id = str(self.playlist_ids)
            
            # Create list of reccomended songs
            recommended_songs = []

            # Iterate through each song in system
            for song_id, song in self.songs.items():
                # If mood is included in mood list
                if(song.mood.lower() in mood_list):
                    # Add song to list
                    recommended_songs.append(song)

            # Shuffle song list
            random.shuffle(recommended_songs)

            # Iterate through each song in list
            counter = 0
            for song in recommended_songs:
                print('adding song to playlist')
                self.add_song_to_playlist(title, song.title)
                
                counter += 1
                if counter == playlist_length:
                    break

        except ValueError as ve:
            raise Exception(f"Invalid argument: {ve}\n in recommend_playlist_based_on_few_moods()")
        except Exception as e:
            raise Exception(f"An error occurred: {e}\n in recommend_playlist_based_on_few_moods()")


    #def create_mood_based_playlist(self, name:str, mood:str): 
        """
        Create a mood based playlist for the user.

        Args:
            user_id (str): ID of the user to create the playlist.
            mood (str): The mood the playlist is based of.

        Returns:
            Playlist: The created mood based playlist.
        """
        #try:
         #   if not isinstance(name, str):
          #      raise ValueError('user_id must be a string')
           # if not isinstance(mood, str):
           #     raise ValueError('mood must be a string')

            # Define a title for the mood based playlist
            #playlist_title = f"{mood} Playlist"

            # Create a new playlist for the user
            #self.register_playlist(name, playlist_title, mood, False)

            # ISSUE HERE TODO
            #playlist_id = str(self.playlist_ids)

            # Iterate through each song in system
            #for song_id in self.songs:
                # If songs mood is 
             #   if(self.songs[song_id].mood.lower() == mood.lower()):
              #      self.add_song_to_playlist(playlist_id=playlist_id, song_id=song_id)

            # Return the new playlist
            #return self.playlists[playlist_id]

        #except ValueError as ve:
         #   raise Exception(f"Invalid argument: {ve}\n in create_mood_based_playlist()")
        #except Exception as e:
         #   raise Exception(f"An error occurred: {e}\n in create_mood_based_playlist()")

         # abondoned!!! 

    def view_timeline(self, username:str) -> str:
        """
        returns a string representation of a given users timeline
        Args:
            username (str): username of user
        Returns:
    	    timeline_overview: overview of all entries in timeline
        """

        try:
            user_id = next((id for id, user in self.users.items() if user.username == username), None)
            if user_id not in self.users:
                raise ValueError("Invalid user ID")

            # Get user
            user = self.users[user_id]

            # Get timeline from user
            timeline = user.mood_timeline

            # Get string representaion of timeline
            timeline_overview = str(timeline)

            # Return the str representation
            return timeline_overview

        except ValueError as ve:
            raise Exception(f"Invalid argument: {ve}\n in view_timeline()")
        except Exception as e:
            raise Exception(f"An error occurred: {e}\n in view_timeline()")


    def view_playlist(self, playlist_id:str) -> str:
        """
        returns a string representation of a given playlist id
        Args:
            playlist_id (str): id of playlist
        Returns:
    	    playlist_overview: overview of all songs in playlist
        """

        try:
            if playlist_id not in self.playlists:
                raise ValueError("Invalid playlist ID")

            # Get Playlist
            playlist = self.playlists[playlist_id]

            # Get string representaion of playlist
            playlist_overview = str(playlist)

            # Return the str representation
            return playlist_overview

        except ValueError as ve:
            raise Exception(f"Invalid argument: {ve}\n in view_playlist()")
        except Exception as e:
            raise Exception(f"An error occurred: {e}\n in view_playlist()")


    def view_song(self, song_id:str) -> str:
        """
        returns a string representation of a given song id
        Args:
            song_id (str): id of song
        Returns:
    	    song_overview: overview of song
        """

        try:
            if song_id not in self.songs:
                raise ValueError("Invalid playlist ID")

            # Get Playlist
            song = self.songs[song_id]

            # Get string representaion of playlist
            song_overview = str(song)

            # Return the str representation
            return song_overview

        except ValueError as ve:
            raise Exception(f"Invalid argument: {ve}\n in view_song()")
        except Exception as e:
            raise Exception(f"An error occurred: {e}\n in view_song()")


    def view_user(self, user_id:str) -> str:
        """
        returns a string representation of a given user id
        Args:
            user_id (str) : id of user
        Returns:
            user_overview: overview of user
        """

        try:
            if user_id not in self.users:
                raise ValueError('invalid user ID')
            
            # Get user
            user = self.users[user_id]

            # Get string representation of user
            user_overview = str(user)

            # return the str representation
            return user_overview

        except ValueError as ve:
            raise Exception(f"Invalid argument: {ve}\n in view_user()")
        except Exception as e:
            raise Exception(f"An error occurred: {e}\n in view_user()")



    def change_mood(self, username:str, mood:str) -> None:
        """
        changes users mood

        Args:
            username (str) : username of user
            mood (str) : mood to be changed
        """

        try:

            user_id = next((id for id, user in self.users.items() if user.username == username), None)
            if user_id not in self.users:
                raise ValueError("Invalid user ID")
            if not isinstance(user_id, str):
                raise ValueError("user_id must be a valid string")
            if not isinstance(mood, str):
                raise ValueError("mood must be a valid string")

            # Get user
            user = self.users[user_id]

            # Change users mood
            user.change_mood(mood)

        except ValueError as ve:
            raise Exception(f"Invalid argument: {ve}\n in change_mood()")
        except Exception as e:
            raise Exception(f"An error occurred: {e}\n in change_mood()")
            
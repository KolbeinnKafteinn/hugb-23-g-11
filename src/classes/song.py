class Song:
    def __init__(self, title:str, artist:str,  mood:str, id:str, lyrics:str = "") -> None:
        self.artist = artist
        self.lyrics = lyrics
        self.title = title
        self.mood = mood
        self.id = id

    def __str__(self) -> str:
        """
        Returns a string representation of object
        """
        return f"{self.artist} - {self.title}"
    
    def json(self) -> dict:
        """
        Returns a json representation of object
        """
        return {
            "artist" : self.artist,
            "lyrics" : self.lyrics,
            "title" : self.title,
            "mood" : self.mood,
            "id" : self.id
        }
class Playlist:
    def __init__(self, author:str, title:str, mood:str, id:str, songs:list, private = True) -> None:
        self.private = private
        self.author = author
        self.songs = songs
        self.title = title
        self.mood = mood
        self.id = id

    def __str__(self) -> str:
        """
        Returns a string representation of object
        """
        songs = "\n".join([str(song) for song in self.songs])
        return f"\t{self.title}\n{songs}\n"

    def json(self) -> dict:
        """
        Returns a json representation of object
        """
        return {
            "private":self.private,
            "author":self.author,
            "title":self.title,
            "mood":self.mood,
            "id":self.id,
            "songs": [x.id for x in self.songs]
        }
    
    def remove(self, song:any) -> None:
        """
        Removes a song from playlist
        
        Args:
            song (Song Object): song object to be removed
        """
        # Remove song from self.songs
        self.songs.remove(song)

    def add(self, song:any) -> None:
        """
        Adds a song to playlist
        
        Args:
            song (Song Object): song object to be added
        """
        # Append item to self.songs list
        self.songs.append(song)

# Mood Music
Welcome to Mood Music—an innovative system that crafts a personalized soundtrack based on your favorite tracks and the emotions they evoke, tailored to your current state and the time of day.

# About 
Mood Music revolutionizes music curation by fusing personal preferences and emotions, dynamically crafting a unique soundtrack aligned with the user's current emotional state, time of day, and context. It offers an endless realm of possibilities, enhancing the depth and personalization of the musical journey for a more enriching experience.

# Installing
```
git clone https://gitlab.com/KolbeinnKafteinn/hugb-23-g-11.git
cd hugb-23-g-11/
pip install -r requirements.txt
```

# Running unittests
```
cd hugb-23-g-11/src/
python -m coverage run -m unittest discover tests
python -m coverage report
python -m coverage html
```

# Running grpc server
```
cd hugb-23-g-11/src/grpc
python server.py
```

# Running grpc client
```
cd hugb-23-g-11/src/grpc
python client.py
```

# Contributors
* Bjarni Gunnar Eiðsson
* Hrafnkell Orri Elvarsson
* Kristinn Anton Hallvarðsson
* Kolbein Þórsson
* Veigar Ágúst Hafþórsson
* Jón Þorri Jónsson

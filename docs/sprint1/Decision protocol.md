# Main Project Decisions
Domain: Mood music

Sprint 2 Scrum Master: Jón

Sprint 3 Scrum Master: Veigar

Sprint 4 Scrum Master: Bjarni

Sprint 5 Scrum Master: Kolbeinn

### 30. aug wednesday
The group met up and presented their User Manual and found out how to best work together.
We decided to hold an extra meeting every monday to work on the current sprint and if needed we would have extra meetings during the week.

### 4. sep monday
We met online through discord and decided to start at least a bit on every part of the sprint.
We then decided before wednesday's meeting to have 1-2 user stories written down that would help us in finding what our project needed.

### 6. sep wednesday
The day before this meeting we noticed we needed to do the requirement list before the anything else (or so we thought), and at the start of the meeting we made a shared google doc to start writing making the requirement list.
After discussing with the TA we found we needed to shift into a faster gear and were lacking a bit in what we were going to make the project into, but after the meeting we did just that and found everything that needed to be done and split it up amongst each team member like so:
    -userstories (Veigar)
    -requirement list (Bjarni)
    -project overview (Kolli / Jón
    -Decision and meetings (Hrafnkell)
    -domain model (Kristinn)
    -Flow chart (optional)
We have now shifted focus into trying to finish most of these things before the next monday meeting and do the final touches or do more optional things during that meeting.

### 11. sep monday
Met on Discord and discussed what needed to be finished for tomorrow.
We decided to optionally put our Requirement list in the Project Overview.
We finished taking out technical items from the Domain Model and put that in the Project Overview.
Afterwards we finished putting the project overview in GitLab.

### 13. sep wednesday
Met in class at around 11 and discussed with TA.
Discussed about the sprint 2 Description.
Talked about what we are starting to do like:
 -selecting a random Scrum Master: Jón
 -choose user stories to work on
 -agree on a DoD
 -divide tasks based on chosen user stories to team members
These are the first parts of the sprint we need to complete before we move on to coding.

### 15. sep friday
Met on Discord and had time poker for each user stories and go the average time each group member thought each user story would take to implement.
The results for the time poker are in the *filepath here*
We also decided on 6 main user stories to work on during this sprint.
Those were:  2, 6, 8, 9, 10, 12
We then split those up into smallers tasks and split group members on a user story or task.
You can see how we split it up in the Issue Boards under Code.
We have not decided completely on the Definition of Done (DoD) but will have it in our next meeting.

### 18. sep monday
Started on the definition of Done
Made some base code for mood and account creation

### 20. sep wednesday
Talked to TA about sprint 1 and we might have forgotten about the rating feature, but we do have a mood rating for each song so might still count.
Started going over sprint 2 progress.
Needed to minimize the user stories we took, so we cut it in half and took: 2, 6 and 9
Which is what we need for the base of the project.
Definition of Done looked good but we need to change it to fit with the user story change and add tests.
We need to start going over gRPC and figure that out before sprint 2 is over.
Optional but wanna add .gitignore file.

### 22. sep friday
Went and fixed the sprint 1 issues the TA mentioned in email.
Removed rating system from scenario 3
Removed user story 18
Change user story 5
We then went over the sprint 2 rubric to see what was missing/being worked on.

### 27. sep wednesday
We will do almost the exact same as the last sprint
At the end of this sprint we need to code review for group: 12

### 29. sep friday
We selected these user stories: 8 and 12

short DoD: 

 -be able to search for songs and play them

 -be able to search for user made playlists

### 1. oct monday
We added the estimated time for user stories in the comments of the user stories
We added tasks for each user story
We talked about how to split tasks and what team members should take them

### 4. oct wednesday
Next time most of us are going to be online we will need to contact the TA to arrange an online meeting
We need to start on the code review and each take 60 minutes and a user story to review
then make a new issue on the board of group 12

### 9. oct monday
Group met up in school after working over the weekend on the project.
We did most of the programming, unittest and gRPC stuff and met up today to finish up the sprint.
We then did the code review for group 12 in our spare time this day.

### 11. oct wednesday
The group is trying out a new system to be more efficient in implementing user stories.
A 3:2:1 method where we have 3 programmers, 2 gRPC-ers, and 1 code reviewer
This is to minimize people waiting on someone else before they can start working as programmers also do unittests for their code, meanwhile gRPC-ers are adding more functionality to it from the last sprint and the code reviewer is reviewing code before merging and making the decision protocol.
This is how the group is split in that new system:
-Programmers: Bjarni, Jón, and Veigar
-gRPC-ers: Kristinn, and Kolbeinn
-Code Reviewer: Hrafnkell
We also have to implement the weather API to a new or existing user story.
We also renamed and refactored some files like renaming moody.py to mood_music.py and also renaming mood.py to timeline.py, we renamed everything in the files that have anything to do with it also.
Next meeting we will select user stories and decide and what to fix from the code review we got from group 9.

### 13. oct friday
We selected these user stories: 10, 13
This user story was selected to implement the weather API: 6
User story 6 pertains to the Timeline so having it there means we can just log weather actively on the timeline and that should make it pretty easy to implement
We decided to fix how we open files from group 9's code review as we did not open them with the "with" statement. We closed them but opening with "with" makes it simpler

### 17. oct tuesday
Group met up and took notes on where everyone was.
Kristinn was working on the weather API for gRPC
Kolbeinn was working on gRPC
Veigar was working on a share functionality
Bjarni and Jón were working on a recommended playlist function
Hrafnkell was merge reviewing, writing DoD and fixing code that didnt work after merging

### 22. oct sunday
Small check in for the project
Sharing and recommended works for the project and passes unit tests after some fixing
Weather API and gRPC is coming along steadily
Some minor things needed to finish the sprint left

### 24. oct tuesday
DoD was added to GitLab and code was reviewed, fixed and merged on to main.
Sprint was gone over and most was added to it.

### 26. oct thursday
Got sprint 3 conditional pass for lacking some comments.
Comments will be fixed soon.

### 28. oct saturday
Got sprint 4 condition pass for weather service not being properly implemented but are not sure how but will ask TA about it.
Also forgot including tasks for one user story but that has been fixed.

### 30. oct monday
Group met up over discord and talked and figured out some questions for the TA for the wednesday meeting.
Where comments are missing.
What's wrong with the weather service.

### 1. nov wednesday
After talking with the TA these are some things we need to fix:
Add the docstrings to the functions missing it.
Weather service needs to be shown in the server.
Add robustness (try catches) for functions that take in input. (So either add try catches or make people select inputs)

We also need to do the Definition of Done

The group then needs to figure out what each member should do and decided on this: 

Bjarni: user story 7, add task breakdown, comments and tests

Jón: user story 1, add task breakdown, comments and tests

Veigar: user story 5, add task breakdown, comments and tests

Kolbeinn: gRPC for the new functions

Kristinn: Comments, try Catch on the gRPC server

Hrafnkell: DoD, CI (Continous Integration), estimate time

### 8-12. nov wednesday

We talked to the TA and showed our progress

It was not good enough so afterwards we got a change from him and Grischa to fix our mistakes

Changes:

We updated this decision protocol along side our changes

We fixed the broken "try-catch"-es in the mood_music.py class and made it so it also registered on the server for every try catch

We made it so in the client it shows both the normal functionality and when an error occurs for most functions

We fixed the timeline so it shows the mood, names of everything and made the weather data actually human readable and more useful

Recommended playlist shows the mood, user, playlist name and songs inside it

We also made it so in most cases where an id was shown it now shows the name/title of it

Added gRPC for delete_user and delete_playlist methods

Added viewing methods for objects so it shows for example: playlists, users, songs and log when the client is run

We also fixed all the unittests that broke with updating the code

We refactored all the tests to fit with our updated code

We fixed a lot of code in the mood_music.py class

some new additions to our mood_music.py class are:

delete_playlist
delete_user
recommend_playlist
log_mood
view_timeline
view_playlist
change_mood

We also fixed the timeline class itself.
